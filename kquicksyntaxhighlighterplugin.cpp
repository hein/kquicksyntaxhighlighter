/*
    SPDX-FileCopyrightText: 2018 Eike Hein <hein@kde.org>

    SPDX-License-Identifier: MIT
*/

#include "kquicksyntaxhighlighterplugin.h"
#include "kquicksyntaxhighlighter.h"

#include <QQmlEngine>

void KQuickSyntaxHighlighterPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(QLatin1String(uri) == QLatin1String("org.kde.kquicksyntaxhighlighter"));

    qmlRegisterType<KQuickSyntaxHighlighter>(uri, 0, 1, "KQuickSyntaxHighlighter");
}
